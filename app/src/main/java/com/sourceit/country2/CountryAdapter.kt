package com.sourceit.country2

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.sourceit.country2.CountryAdapter.CountryHolder
import com.sourceit.country2.network.model.Country
import kotlinx.android.synthetic.main.item_country.view.*
import java.util.*

class CountryAdapter(mainActivity: MainActivity) : RecyclerView.Adapter<CountryHolder>() {
    private val originalList: MutableList<Country> = ArrayList()
    private var filterList: MutableList<Country> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_country, parent, false)
        return CountryHolder(itemView)
    }

    override fun onBindViewHolder(holder: CountryHolder, position: Int) {
        val country: Country = filterList[position]
        holder.name.text = String.format("%s", country.name)
        holder.region.text = String.format("%s", country.region)
        holder.area.text = String.format("area: %s ", country.area)
        holder.population.text = String.format("population: %s ", country.population)
        GlideToVectorYou
                .init()
                .with(holder.flag.context)
                .load(Uri.parse(country.flag), holder.flag)
    }

    override fun getItemCount() = filterList.size

    fun update(countries: List<Country>) {
        originalList.clear()
        originalList.addAll(countries)
        filterList.clear()
        filterList.addAll(countries)
        notifyDataSetChanged()
    }

    fun filterByPopulation(inputPopulation: String) {
        filterList.clear()
        if (inputPopulation == "") {
            filterList.addAll(originalList)
        } else {
            val inputPopulationInInt = inputPopulation.toInt()
            filterList = originalList.filter { it.population in ((inputPopulationInInt * 0.85).toInt()..(inputPopulationInInt * 1.15).toInt()) } as MutableList<Country>
        }
    }

    fun filterByArea(inputArea: String) {
        filterList.clear()
        if (inputArea == "") {
            filterList.addAll(originalList)
        } else {
            val inputAreaInDouble = inputArea.toDouble()
            filterList = originalList.filter { it.area in (inputAreaInDouble * 0.85)..(inputAreaInDouble * 1.15) } as MutableList<Country>
        }
    }

    fun filterByRegion(inputRegion: String) {
        filterList.clear()
        if (inputRegion == "") {
            filterList.addAll(originalList)
        } else {
            filterList = originalList.filter { it.region.contains(inputRegion) } as MutableList<Country>
        }
    }

    class CountryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.name
        val area: TextView = itemView.area
        val region: TextView = itemView.region
        val population: TextView = itemView.population
        val flag: ImageView = itemView.flag
    }

}