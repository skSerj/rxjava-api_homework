package com.sourceit.country2;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou;
import com.sourceit.country2.databinding.ItemCountryBinding;
import com.sourceit.country2.network.model.Country;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryHolder> {
    private Context context;
    private List<Country> originalList = new ArrayList<>();
    private List<Country> filterList = new ArrayList<>();

    public CountryAdapter(Context context) {
        this.context = context;
    }


    @NonNull
    @Override
    public CountryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCountryBinding binding = ItemCountryBinding.inflate(LayoutInflater.from(context), parent, false);
        return new CountryHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryHolder holder, int position) {
        Country country = filterList.get(position);
        holder.binding.name.setText(String.format("%s", country.getName()));
        holder.binding.region.setText(String.format("%s", country.getRegion()));
        holder.binding.area.setText(String.format("area: %s ", country.getArea()));
        holder.binding.population.setText(String.format("population: %s ", country.getPopulation()));
        GlideToVectorYou
                .init()
                .with(holder.binding.flag.getContext())
                .load(Uri.parse(country.getFlag()), holder.binding.flag);
    }

    @Override
    public int getItemCount() {
        return filterList.size();
    }


    public void update(List<Country> countries) {
        originalList.clear();
        originalList.addAll(countries);

        filterList.clear();
        filterList.addAll(countries);
        notifyDataSetChanged();
    }

    public void filterByPopulation(String inputPopulation) {
        filterList.clear();
        if (inputPopulation.equals("")) {
            filterList.addAll(originalList);
        } else {
            int inputPopulationInInt = Integer.parseInt(inputPopulation);
            for (Country country : originalList) {
                if (country.getPopulation()> (inputPopulationInInt * 0.85) && country.getPopulation() < (inputPopulationInInt * 1.15)) {
                    filterList.add(country);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void filterByArea(String inputArea) {
        filterList.clear();
        if (inputArea.equals("")) {
            filterList.addAll(originalList);
        } else {
            double inputAreaInDouble = Double.parseDouble(inputArea);
            for (Country country : originalList) {
                if (country.getArea() != null) {
                    if (country.getArea() > (inputAreaInDouble * 0.85d) && country.getArea() < (inputAreaInDouble * 1.15d)) {
                        filterList.add(country);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public void filterByRegion(String inputRegion) {
        filterList.clear();
        if (inputRegion.equals("")) {
            filterList.addAll(originalList);
        } else {
            for (Country country : originalList) {
                if (Objects.requireNonNull(country.getRegion()).contains(inputRegion.toLowerCase())) {
                    filterList.add(country);
                }
            }
        }
        notifyDataSetChanged();
    }

    public static class CountryHolder extends RecyclerView.ViewHolder {
        public ItemCountryBinding binding;

        public CountryHolder(ItemCountryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
