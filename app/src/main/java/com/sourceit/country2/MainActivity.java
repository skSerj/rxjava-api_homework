package com.sourceit.country2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.sourceit.country2.databinding.ActivityMainBinding;
import com.sourceit.country2.network.ApiService;
import com.sourceit.country2.network.model.Country;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private Disposable disposable;
    private ActivityMainBinding binding;
    private CountryAdapter adapter;
    public static long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.countryRecyclerList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CountryAdapter(this);
        binding.countryRecyclerList.setAdapter(adapter);
        binding.btnFilterPopulation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputPopulation = binding.inputFilterData.getText().toString();
                adapter.filterByPopulation(inputPopulation);
            }
        });
        binding.btnFilterArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputArea = binding.inputFilterData.getText().toString();
                adapter.filterByArea(inputArea);
            }
        });
        binding.btnFilterRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputRegion = binding.inputFilterData.getText().toString();
                adapter.filterByRegion(inputRegion);
            }
        });
        disposable = (Disposable) ApiService.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showInfo, this::showError);
    }


    private void showInfo(List<Country> countries) {
        adapter.update(countries);
    }

    private void showError(Throwable throwable) {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, "Press once again to exit!", Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (disposable != null) {
            disposable.dispose();
        }
    }
}
