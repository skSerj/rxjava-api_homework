package com.sourceit.country2.network.model

data class Country(val name: String, val flag: String, val region: String, val area: Double, val population: Int) {

}