package com.sourceit.country2.network.model;

public class Country {
    public String name;
    public String flag;
    public String region;
    public Double area;
    public Integer population;
}
