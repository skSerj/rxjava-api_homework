package com.sourceit.country2

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.sourceit.country2.network.ApiService.data
import com.sourceit.country2.network.model.Country
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var disposable: Disposable? = null
    private lateinit var adapter: CountryAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        country_recycler_list.adapter = CountryAdapter(this)
        country_recycler_list.layoutManager = LinearLayoutManager(this)

        btn_filter_population.setOnClickListener {
            val inputPopulation = input_filter_data.text.toString()
            adapter.filterByPopulation(inputPopulation)
        }
        btn_filter_area.setOnClickListener {
            val inputArea = input_filter_data.text.toString()
            adapter.filterByArea(inputArea)
        }
        btn_filter_region.setOnClickListener {
            val inputRegion = input_filter_data.text.toString()
            adapter.filterByRegion(inputRegion)
        }
        disposable = data
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ countries: List<Country> -> showInfo(countries) }) { throwable: Throwable -> showError(throwable)}
    }


    private fun showInfo(countries: List<Country>) {
        adapter.update(countries)
    }

    private fun showError(throwable: Throwable) {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
        } else {
            Toast.makeText(this, "Press once again to exit!", Toast.LENGTH_SHORT).show()
        }
        back_pressed = System.currentTimeMillis()
    }

    override fun onStop() {
        super.onStop()
        disposable?.dispose()
    }

    companion object {
        var back_pressed: Long = 0
    }
}